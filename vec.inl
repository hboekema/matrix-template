//
//  vec_impl.h
//  mat_template
//
//  Created by Hidde Boekema on 10/03/2018.
//  Copyright © 2018 Hidde Boekema. All rights reserved.
//

#ifndef vec_inl
#define vec_inl

#include <vector>
#include <cassert>
#include "vec.h"
#include "mat_base.h"
#include "checks.h"
#include "global.h"

template <class type> vec<type>::vec(const std::vector<type>& V, const bool& T):
Base<type>(V, (uint) V.size(), 1)
{   // Class constructor. T specifies whether the vector has been transposed or not.
    if (T == true) { this->m_Rows = 1; this->m_Columns = (uint) V.size(); }
}

template <class type> inline vec<type> vec<type>::Trans() const
{   // Returns the transpose of a vector
    const vec<type>* p_transpose = Transpose();
    const vec<type> transpose = *p_transpose;
    delete p_transpose; // Ensure the heap pointer is destroyed (dangling pointer not an issue here)
    return transpose;
}

template <class type> const vec<type>* vec<type>::Transpose() const
{   // Returns a pointer to the transpose of the vector
    vec<type> temp(this->Members, true);    // A vector has just one column so simply switching the rows and columns gives the transpose
    
    // Return a pointer to the literal
    vec<type>* p_temp = new vec<type>(temp);
    return p_temp;
}

template <class type> float vec<type>::operator*(const vec<type>& other) const
{   // Dot products two vectors - essentially normal matrix multiplication
    assert(this->m_Rows == other.m_Rows && this->m_Columns == other.m_Columns);
    
    float sum;
    
    for (uint i = 0; i < this->m_Rows; i++)
    {
        for (uint j = 0; j < this->m_Columns; j++)  // Two loops should not be needed but if the vectors were transposed it is possible that the summation should happen over columns instead of rows
        {
            sum += (*this)[i][j] * other[i][j];
        }
    }
    
    return sum;
}

template <class type> vec<type> vec<type>::operator^(const vec<type>& other) const
{   // Cross products two vectors
    assert(this->m_Rows == other.m_Rows && this->m_Columns == other.m_Columns);
    
    vec<type> temp;
    
    for (uint i = 0; i < this->m_Rows; i++)
    {
        for (uint j = 0; j < this->m_Rows; j++)
        {
            
        }
    }
}

#endif /* vec_inl */
