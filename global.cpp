//
//  global.cpp
//  mat_template
//
//  Created by Hidde Boekema on 25/02/2018.
//  Copyright © 2018 Hidde Boekema. All rights reserved.
//

#include "global.h"

#include <typeinfo>
#include <iostream>
#include <vector>

// Definitions of the variables and functions declared in global.h

// Global constant pointers to the type_info of standard types
const std::type_info* type_bool = &typeid(bool);
const std::type_info* type_double = &typeid(double);
const std::type_info* type_int = &typeid(int);
const std::type_info* type_long = &typeid(long);
const std::type_info* type_short = &typeid(short);
const std::type_info* type_uint = &typeid(uint);
const std::type_info* type_ulong = &typeid(u_long);
const std::type_info* type_ushort = &typeid(ushort);

// Cluster special/similar types
const std::vector<const std::type_info*> all_types = {type_bool, type_double, type_int, type_long, type_short, type_uint, type_ulong, type_ushort};
const std::vector<const std::type_info*> numerical_types = {type_double, type_int, type_long, type_short, type_uint, type_ulong, type_ushort};

// Classless functions that may be of use
std::vector<uint> create_identity(const uint& n)
{   // Creates and returns an n * n identity matrix vector
    std::vector<uint> I_vector;
    
    for (uint i = 0; i < n; i++)
    {
        for (uint j = 0; j < n; j++)
        {
            if (i == j)
            {   // If along the leading diagonal, this entry should be 1. Otherwise, it should be 0.
                I_vector.push_back(1);
            } else {
                I_vector.push_back(0);
            }
        }
    }
    
    return I_vector;
}
