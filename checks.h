//
//  checks.h
//  Matrix Template
//
//  Created by Hidde Boekema on 24/02/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#ifndef checks_h
#define checks_h

#include <typeinfo>
#include <cassert>
#include <vector>

void typecheck(const std::type_info* type, const std::vector<const std::type_info*>& types_allowed);
void sizecheck(const double& size, const double& min_size = 0, const double& max_size = __DBL_MAX__);
bool zero(const double& value);

#endif
