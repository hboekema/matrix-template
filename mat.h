//
//  mat.h
//  Matrix Template
//
//  Created by Hidde Boekema on 30/01/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#ifndef mat_h
#define mat_h

#include <vector>
#include <cmath>
#include <cassert>
#include "vec.h"

template <class type> class mat : public Base<type>
{
    // Derived class for N by M matrices
public:
    mat(const std::vector<type>& V = {}, const uint& N = 0, const uint& M = 0);
    
    // Overloaded operators for matrix operations
    mat operator*(const mat& other) const;
    mat operator+(const mat& other) const;
    mat operator-(const mat& other) const;
    
    // Overloaded operators for scalar operations
    mat operator*(const type& t) const;
    mat operator/(const type& t) const;
    
    // Member functions to find the determinant, inverse and transpose of a 3 by 3 matrix
    type Det() const;
    type SlowDet() const;
    mat Trans() const;
    mat Inv() const;
    int Rank() const;
    std::vector<double> Eigenvalues() const;
    std::vector<double> Eigenvectors() const;
    mat RowEchelon(const bool& reduced = true) const;
    std::vector<mat> LUDecomposition() const;
    
private:
    /* When one of the above operations is called, store the result in one of these variables for faster access to the matrix properties */
    double determinant;
    int rank;
    std::vector<double> eigenvalues;
    std::vector<vec<double>> eigenvectors;
    
    /* Functions needed for the above matrix operations only - Transpose() is needed to allow Trans() to  return a mat object instead of a pointer to a mat object */
    mat CoInv() const;
    mat GJInv() const;
    const double Trace() const;
    const mat* Transpose() const;
    mat Cofactors() const;
    mat RemoveRC(const uint& row, const uint& column) const; // Creates a matrix equal to the full matrix without row row and column column
    mat Adjugate() const;
    mat Pivot(const uint& startpos = 0, const uint& pivot_in_columns = 0, const int& force_pivot = -1) const;
    void MultiplyRow(const uint& row, const double& value);
    void DivideRow(const uint& row, const double& value);
    void AddRows(const uint& r1, const uint& r2);
    void SubtractRows(const uint& r1, const uint& r2);
    const uint FirstInRC(const double& number = 0, const uint& startpos = 0, const bool& R = true, const bool& C = false) const;  // Used in Inv() to find the row a matrix has been pivoted over
};

#include "mat.inl"

#endif /* mat_h */
