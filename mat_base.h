//
//  mat_base.h
//  mat_template
//
//  Created by Hidde Boekema on 16/04/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#ifndef mat_base_h
#define mat_base_h

#include <iostream>
#include <vector>
#include "checks.h"
#include "global.h"

// Header file with (abstract) template base class declarations - note that this file #include's the definitions of the functions of this class

template <class type> class Base
{
    // Pure virtual base class
public:
    /* Class constructor, functions for retrieving data members and virtual function for finding the transpose of a matrix */
    friend std::ostream& operator<<(std::ostream& os, const Base<type>& mat)
    {   // Overload the stream insertion operator for use with Base and mat
        os << std::endl;
        
        for (uint i = 0; i < mat.Get_Rows(); i++)
        {
            os << "[ ";
            for (uint j = 0; j < mat.Get_Columns(); j++)
            {
                os << mat.Get_Member(i, j) << " ";
            }
            os << "]" << std::endl;
        }
        
        return os;
    }
    
    // Explicitly defined operators and constructors
    Base(const std::vector<type>& M, const uint& R, const uint& C);
    Base(const Base& to_copy);
    Base& operator=(const Base& matrix);
    Base& operator=(const Base&& matrix);
    
    type Get_Member(const int& i, const int& j = 0) const;    // A different, safer way of accessing a member
    std::vector<type> Get_Members() const;
    uint Get_Rows() const;
    uint Get_Columns() const;
    void Set_RC(const uint& R, const uint& C);
    void Edit_Member(const uint& i, const uint& j, const type& value);      // Edit the value of the specified member
    void Increment_Member(const uint& i, const uint& j, const type& value);
    void Multiply_Member(const uint& i, const uint& j, const type& value);
    void Erase(const bool& updateRC = false);
    virtual const Base* Transpose() const = 0;
    
    class Proxy
    {   // Proxy class for dual indexing using subscripting operator
    public:
        friend std::ostream& operator<<(std::ostream& os, const Proxy& proxy)
        {   // Overload the stream insertion operator for use with Proxy
            std::vector<type> mat_row = proxy.m_Mat_Row;
            
            os << "[ ";
            for (typename std::vector<type>::iterator iter = mat_row.begin(); iter < mat_row.end(); iter++)
            { os << *iter << " "; }
            
            os << "]" << std::endl;
            return os;
        }
        
        Proxy(const std::vector<type>& R);
        type operator[](const int& j) const;
        
    private:
        /* This will store the row i indexed by the user in the overloaded subscripting operator in the
         Base class */
        std::vector<type> m_Mat_Row;
    };
    
    // Overloaded subscripting operator
    Proxy operator[](const int& i) const;
    
protected:
    uint m_Rows;
    uint m_Columns;
    uint m_Size;
    std::vector<type> Members;
    
    void UpdateSize(const bool& updateRC = false);
};

#include "mat_base.inl"

#endif /* mat_base_h */

