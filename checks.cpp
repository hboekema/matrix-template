//
//  checks.cpp
//  mat_template
//
//  Created by Hidde Boekema on 25/02/2018.
//  Copyright © 2018 Hidde Boekema. All rights reserved.
//

#include "checks.h"

#include <typeinfo>
#include <cassert>
#include <vector>

inline void typecheck(const std::type_info* type, const std::vector<const std::type_info*>& types_allowed)
{   // Checks if the type provided to a generic function/template class function is permissible
    /* Use the type_info::typeid() function to obtain the type information for both type and types_allowed and convert this to a pointer - this will return a type_info* type - and pass these as arguments
     
     Example code:
     
     int main()
     {
     const std::type_info* type_double = &typeid(double);
     const std::type_info* type_int = &typeid(int);
     
     const std::vector<const std::type_info*> allowed = {type_int, type_double};
     typecheck(type_double, allowed);              // Assertion passes
     
     const std::vector<const std::type_info*> allowed = {type_int};
     typecheck(type_double, allowed);              // Assertion fails
     
     return 0;
     }
     
     In order for this to work correctly, the header <typeinfo> must be included in the program using this check - it is included in this header for convenience.
     */
    assert(types_allowed.size() > 0);
    bool temp = false;
    
    for (std::vector<const std::type_info*>::const_iterator iter = types_allowed.begin(); iter < types_allowed.end(); iter++)
    {   // The type given is acceptable
        if (type == *iter) { temp = true; }
    }
    assert(temp);   // If this fails, the type given was not compatible with the function used
}

inline void sizecheck(const double& size, const double& min_size, const double& max_size)
{   // Checks if the size given falls within the permitted range. Returns true if this condition is met.
    assert(size > min_size && size < max_size);
}

inline bool zero(const double& value)
{   // Checks if the argument is zero. Returns true if it is.
    return value == 0;
}

