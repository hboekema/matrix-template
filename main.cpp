//
//  main.cpp
//  Matrix Template
//
//  Created by Hidde Boekema on 30/01/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <vector>
#include "mat.h"
#include "vec.h"

int main(int argc, const char * argv[]) {
    
    uint dof = 3;
    std::vector<uint> v1 = create_identity(dof);
    mat<uint> m1(v1, dof, dof);
    
    clock_t t1 = clock();
    std::cout << m1.Det() << std::endl;
    double time = double(clock() - t1)/CLOCKS_PER_SEC;
    std::cout << "Time elapsed: " << time << "s." << std::endl;
    
    std::vector<double> v2 = {1, 2, 1, -2, -3, 1, 3, 5, 0};
    mat<double> m2(v2, 3, 3);
    std::cout << m2 << std::endl;
    clock_t c = clock();
    //std::cout << m2.Rank() << std::endl;
    clock_t clocks = (clock() - c);
    double time2 = ((double)clocks)/CLOCKS_PER_SEC;
    std::cout << "Time elapsed: " << time2 << "s." << std::endl;

    return 0;
}
