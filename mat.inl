//
//  mat_impl.h
//  mat_template
//
//  Created by Hidde Boekema on 10/03/2018.
//  Copyright © 2018 Hidde Boekema. All rights reserved.
//

#ifndef mat_inl
#define mat_inl

#include "mat.h"

#include <vector>
#include <cmath>
#include <cassert>
#include "vec.h"
#include "mat_base.h"
#include "checks.h"
#include "global.h"

template <class type> mat<type>::mat(const std::vector<type>& V, const uint& N, const uint& M):
Base<type>(V, N, M)
{}

template <class type> mat<type> mat<type>::operator*(const mat<type>& other) const
{
    mat<type> temp;
    for (int i = 0; i < this->m_Rows; i++)
    {
        for (int j = 0; j < this->m_Columns; j++)
        {
            for (int k = 0; k < 3; k++)
            {
                temp.Members[this->m_Rows * i + j] += (*this)[i][k] * other[k][j];
            }
        }
    }
    return temp;
}

template <class type> mat<type> mat<type>::operator+(const mat<type>& other) const
{
    mat<type> temp;
    for (int i = 0; i < this->m_Rows; i++)
    {
        for (int j = 0; j < this->m_Columns; j++)
        {
            temp.Members[this->m_Rows * i + j] = (*this)[i][j] + other[i][j];
        }
    }
    return temp;
}

template <class type> mat<type> mat<type>::operator-(const mat<type>& other) const
{
    mat<type> temp;
    for (int i = 0; i < this->m_Rows; i++)
    {
        for (int j = 0; j < this->m_Columns; j++)
        {
            temp.Members[this->m_Rows * i + j] = (*this)[i][j] - other[i][j];
        }
    }
    return temp;
}

template <class type> mat<type> mat<type>::operator*(const type& t) const
{
    std::vector<type> temp;
    std::vector<type> members = this->Members;
    for (typename std::vector<type>::const_iterator i = members.begin(); i != members.end(); i++)
    {
        temp.push_back(*i * t);
    }
    
    mat<type> mat_temp(temp, this->m_Rows, this->m_Columns);
    return mat_temp;
}

template <class type> mat<type> mat<type>::operator/(const type& t) const
{
    std::vector<type> temp;
    std::vector<type> members = this->Members;
    for (typename std::vector<type>::const_iterator i = members.begin(); i != members.end(); i++)
    {
        temp.push_back(*i / t);
    }
    
    mat<type> mat_temp(temp, this->m_Rows, this->m_Columns);
    return mat_temp;
}

template <class type1, class type2> inline mat<type1> operator*(const type2& t, const mat<type1>& matrix)
{   // Overloaded multiplication operator in order to achieve commutative scalar multiplication
    return matrix * t;
}

template <class type> type mat<type>::Det() const
{   /* Returns the determinant of a matrix efficiently using elementary formulae or LU decomposition. The latter does not work for singular matrices, in which case SlowDet() must be used */
    typecheck(&typeid(type), numerical_types);
    
    uint rows = this->m_Rows;
    uint columns = this->m_Columns;
    assert(rows == columns);
    
    const mat<type> matrix = *this;
    
    if (rows == 1)
    {   // Determinant is the number in the matrix
        return matrix[0][0];
    } else if (rows == 2) {
        // Find determinant using ad - bc
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0];
    } else if (rows == 3) {
        // Find determinant using a(ei - fh) - b(di - fg) + c(dh - eg)
        return matrix[0][0] * (matrix[1][1] * matrix[2][2] - matrix[1][2] * matrix[2][1]) - matrix[0][1] * (matrix[1][0] * matrix[2][2] - matrix[1][2] * matrix[2][0]) + matrix[0][2] * (matrix[1][0] * matrix[2][1] - matrix[1][1] * matrix[2][0]);
    } else {
        // Find determinant using LU decomposition - this is slower than the above formulae
        const std::vector<mat<type>> temp = LUDecomposition();
        
        // Det(A) = Det(L) * Det(U) = product of traces of L and U, but note that trace of L is 1
        return temp[1].Trace();
    }
}

template <class type> type mat<type>::SlowDet() const
{   /* The slower alternative to Det() which is guaranteed to correctly calculate the determinant of a matrix or notify the user if the matrix is singular */
    throw std::runtime_error("Function unfinished.");
}

template <class type> inline mat<type> mat<type>::Trans() const
{   // Return dereferenced pointer to the transpose of the matrix
    return *(Transpose());
}

template <class type> inline mat<type> mat<type>::Inv() const
{   // Function accessable to user. Automatically chooses the fastest method to invert the matrix.
    uint rows = this->m_Rows;
    uint columns = this->m_Columns;
    assert(rows == columns);
    if (rows == 1 || rows == 2) { return GJInv(); } else if (rows > 2 && rows < 10) { return CoInv(); } else { return GJInv(); }
}

template <class type> int mat<type>::Rank() const
{   // Finds and returns the rank of the matrix
    uint rows = this->m_Rows;
    uint columns = this->m_Columns;
    int rank = rows;
    mat<type> REmatrix((RowEchelon()).Members, rows, columns);
    
    for (uint i = 0; i < rows; i++)
    {
        double sum = 0;
        for (uint j = 0; j < columns; j++) { sum += REmatrix[i][j]; }
        if (sum == 0) { rank -= 1; }
    }
    return rank;
}

template <class type> std::vector<double> mat<type>::Eigenvalues() const
{   // Finds the eigenvalues of the given matrix
    throw std::runtime_error("Function unfinished.");
}

template <class type> mat<type> mat<type>::RowEchelon(const bool& reduced) const
{   // Function that calculates the row echelon or reduced row echelon form of a matrix
    mat<type> matrix = *this;
    uint rows = matrix.m_Rows;
    uint columns = matrix.m_Columns;
    
    uint smaller = 0;
    if (rows < columns) { smaller = rows; } else { smaller = columns;}  // See below.
    
    // Perform the elimination
    for (uint j = 0; j < smaller; j++)  /* Since this is a row operation, j denotes a row number. However, to avoid attempts at accessing members that don't exist, j must be smaller than the smaller of rows or columns. */
    {
        // Pivot the matrix first if necessary
        if (matrix[j][j] == 0) { matrix = matrix.Pivot(j);   /* Avoid pivoting with the rows above */ }
        
        type entry = matrix[j][j];
        if (reduced && entry != 0)
        {   // Prepare matrix for elimination
            matrix.DivideRow(j, entry);
        }
        
        for (uint i = 0; i < rows; i++)
        {
            if (i != j)
            {   double factor = matrix[i][j];   // Factor that will be used to multiply the row
                if (factor != 0)
                {   // Multiply row
                    matrix.MultiplyRow(j, factor);
                    
                    // Perform subtraction
                    matrix.SubtractRows(i, j);
                    
                    // Restore row
                    matrix.DivideRow(j, factor);
                }
            }
        }
    }
    return matrix;
}

template <class type> std::vector<mat<type>> mat<type>::LUDecomposition() const
{   // Find the upper-triangular and lower-triangular matrices for use in the LU(P) decomposition
    // Works for most, but not all, nonsingular matrices
    std::vector<type> u;
    std::vector<type> l;
    uint rows = this->m_Rows;
    uint columns = this->m_Columns;
    mat<type> temp;
    
    // Check if matrix is suitable
    if ((*this)[0][0] == 0)
    {   // Perform partial pivoting
        temp = Pivot();
    } else { // Do nothing
        temp = *this;
    }
    /* assert(SlowDet() != 0); - this check is necessary but defeats the purpose of using LU decomposition - yet to be solved */
    
    for (uint i = 0; i < rows; i++)
    {
        for (uint j = 0; j < columns; j++)
        {   // Fill with zeros so that values can be changed by indexing
            u.push_back(0);
            l.push_back(0);
        }
    }
    
    for (uint i = 0; i < rows; i++)
    {
        for (uint j = 0; j < columns; j++)
        {   // Account for all possible positions in both L and U
            if (i == 0)
            {   // This result is true - easily proven by evaluation of A = LU
                u[i * columns + j] = temp[0][j];
                
                /* Evaluate L - this is easy to do since it is a row with just one non-zero entry (1) as the first entry */
                if (j == 0)
                {
                    l[i * columns + j] = 1;
                } else { /* Do nothing; there is already a zero here */ }
                
            } else if (j == 0) {
                // This is very easy to compute due to the patterns of 1's and 0's in the matrices
                l[i * columns + j] = temp[i][j]/u[0];
                
                // U does not need to be evaluated for this case as all the right values are already in place
                
            } else if (i > j) {
                // If in the lower echelon of the matrix, U[i][j] = 0
                double sum = 0;
                
                for (uint k = 0; k < j; k++)
                {   // Find all terms
                    sum += l[i * columns + k] * u[k * columns + j];
                }
                
                /* Determined through the LU decomposition of a general 4 * 4 matrix and extending to n dimensions */
                if (u[j * rows + j] != 0)
                {   // Check for division by zero
                    l[i * columns + j] = (temp[i][j] - sum)/u[j * rows + j];
                } else {
                    // Do nothing -> l[i][j] already zero
                }
                
            } else if (j > i) {
                // If in the upper echelon, L[i][j] = 0
                double sum = 0;
                
                for (uint k = 0; k < j; k++)
                {
                    sum += l[i * columns + k] * u[k * columns + j];
                }
                
                // Same as above
                u[i * columns + j] = temp[i][j] - sum;
                
            } else if (i == j) {
                // If on the leading diagonal, L[i][j] = 1
                l[i * columns + j] = 1;
                
                /* This formula was determined by evaluating a general 3 * 3 matrix and extending to n dimensions */
                double sum = 0;
                
                // Make use of the fact that i equals j to find the summation of L[i][k] * U[k][j]
                for (uint k = 0; k <= i; k++)
                {
                    sum += l[i * columns + k] * u[k * columns + j];
                }
                
                u[i * columns + j] = temp[i][j] - sum;
                
            } else {
                // This should never be called since the cases above are exhaustive
            }
        }
    }
    
    mat<type> U(u, rows, columns);
    mat<type> L(l, rows, columns);
    std::vector<mat<type>> L_and_U = {L, U};
    return L_and_U;
}

template <class type> inline mat<type> mat<type>::CoInv() const
{   /* Return the inverse of an n * n matrix using cofactors - very slow for large matrices and should thus be used for small matrices only! */
    double det = Det();
    if (det != 0) { return Adjugate()/det; }
    else { std::cout << "Inverse does not exist. Original matrix returned. " << std::endl; return *this; }
}

template <class type> mat<type> mat<type>::GJInv() const
{   /* Calculates the inverse of a matrix using Gauss-Jordan elimination - fast for large matrices but slower than Inv() for small matrices */
    const uint rows = this->m_Rows;
    const uint columns = this->m_Columns;
    
    /* This is the identity matrix on the right-hand side of the augmented matrix used in Gauss-Jordan elimination. Anything done to the left-hand side (i.e. matrix whose inverse is desired) will be done to this matrix. At the end of the elimination, this matrix will be the inverse. */
    mat<type> identity(create_identity, rows, columns);
    mat<type> matrix = *this;
    
    // Perform the elimination
    for (uint j = 0; j < columns; j++)
    {
        if (matrix[j][j] == 0) { // Pivot the matrix first
            matrix = matrix.Pivot(j);   // Avoid pivoting with the rows above
            identity = identity.Pivot(j, matrix.FirstInRC(0, j));     // Force identity to pivot with same row
        }
        
        // Prepare matrix for elimination
        type entry = matrix[j][j];
        matrix.DivideRow(j, entry);
        identity.DivideRow(j, entry);
        
        for (uint i = 0; i < rows; i++)
        {
            if (i != j)
            {   double factor = matrix[i][j];   // Factor that will be used to multiply the row
                if (factor != 0)
                {   // Multiply row
                    matrix.MultiplyRow(j, factor);
                    identity.MultiplyRow(j, factor);
                    
                    // Perform subtraction
                    matrix.SubtractRows(i, j);
                    identity.SubtractRows(i, j);
                    
                    // Restore row
                    matrix.DivideRow(j, factor);
                    identity.DivideRow(j, factor);
                }
            }
        }
    }
    // Return the inverse
    return identity;
}

template <class type> inline const double mat<type>::Trace() const
{   // Finds the trace of a matrix
    double product = 1;
    const mat<type> matrix = *this;
    
    for (uint i = 0; i < this->m_Rows; i++)
    {   // The trace of a matrix is the product of the entries of the leading diagonal
        product *= matrix[i][i];
    }
    
    return product;
}

template <class type> const mat<type>* mat<type>::Transpose() const
{   // Returns pointer to the transpose of an n * n matrix
    uint n = this->m_Rows;
    uint m = this->m_Columns;
    
    assert(n == m);
    
    // Create an empty matrix object and a pointer to it
    std::vector<type> t;
    t = this->Members;
    mat<type> temp(t, n, m);
    mat<type>* p_temp = new(mat<type>);
    
    for (uint i = 0; i < n; i++)
    {
        for (uint j = 0; j < m; j++)
        {   // Check bounds and proceed to switch
            if (i != j)
            {
                // Switch row and column
                temp.Members[n * i + j] = (*this)[j][i];
                
            } else {
                // Do nothing
                continue;
            }
        }
    }
    
    // Assign temp to the address pointed to by p_temp
    *p_temp = temp;
    // Return the pointer; this is dereferenced by Trans()
    return p_temp;
}

template <class type> mat<type> mat<type>::Cofactors() const
{   /* Find the matrix of cofactors such that the adjugate and determinant of the matrix may be found easily. This function can be very slow for large matrices due to the cyclic nature of the process used */
    const uint rows = this->m_Rows;
    const uint columns = this->m_Columns;
    
    std::vector<type> values;
    std::vector<type> temp_vec;
    mat<type> temp(temp_vec, rows - 1, columns - 1);
    
    // Iterate through the original matrix, finding cofactors and storing these
    for (uint i = 0; i < rows; i++)
    {
        for (uint j = 0; j < columns; j++)
        {
            // Reduce the matrix to contain just the desired rows and columns
            temp = RemoveRC(i, j);
            
            // Get determinant of reduced matrix
            values.push_back(pow(-1, i + j) * temp.Det());
            
            temp.Erase();
        }
    }
    
    mat<type> cofactors(values, rows, columns);
    return cofactors;
}

template <class type> mat<type> mat<type>::RemoveRC(const uint& row, const uint& column) const
{   // Remove a row and column - useful for cofactors
    const std::vector<type> v_large = this->Members;
    const uint rows = this->m_Rows;
    const uint columns = this->m_Columns;
    
    std::vector<type> v_small;
    
    for (uint i = 0; i < rows; i++)
    {
        if (i != row)
        {
            for (uint j = 0; j < columns; j++)
            {
                if (j != column)
                {
                    v_small.push_back(v_large[i * columns + j]);
                }
            }
        }
    }
    
    mat<type> temp(v_small, rows - 1, columns - 1);
    return temp;
}

template <class type> inline mat<type> mat<type>::Adjugate() const
{   // Function to find the adjugate of a matrix using the matrix of minors method
    // Transpose the matrix of cofactors to obtain the adjugate
    return (Cofactors()).Trans();
}

template <class type> mat<type> mat<type>::Pivot(const uint& startpos, const uint& pivot_in_column, const int& force_pivot) const
{   /* Function that performs a partial (specifically, row) pivot - if force_pivot is used, the function will perform the pivot with the row corresponding to the argument provided; if pivot_in_column is used, the pivot will be performed in the column specified */
    if ((*this)[0][pivot_in_column] != 0 && force_pivot < 0)
    {
        return *this;
    }
    
    const uint rows = this->m_Rows;
    const uint columns = this->m_Columns;
    uint row_to_pivot = 0;
    
    if (force_pivot < 0 || force_pivot >= rows)
    {
        uint i = startpos;
        for (uint  z = startpos; z < rows; z++) { if ((*this)[z][pivot_in_column] == 1) { row_to_pivot = z; } }
        while (row_to_pivot == 0) {    // This is where the row to switch with is decided
            i++;
            if ((*this)[i][pivot_in_column] != 0) { row_to_pivot = i; } else if (i == rows) { std::cout << "Pivot failed." << std::endl; return *this; }
        }
    } else { row_to_pivot = force_pivot; }
    assert(row_to_pivot >= 0 && row_to_pivot < rows);
    
    std::vector<type> temp = this->Get_Members();
    for (uint j = 0; j < columns; j++)
    {
        temp[j] = (*this)[row_to_pivot][j];     // Change the (j + 1)th element in the 1st row
        temp[row_to_pivot * columns + j] = (*this)[0][j];
    }
    
    mat<type> pivoted_mat(temp, rows, columns);
    return pivoted_mat;
}

template <class type> void inline mat<type>::MultiplyRow(const uint& row, const double& value)
{   // Multiplies the given row by the value provided
    for (uint z = 0; z < this->m_Columns; z++) { Edit_Member(row, z, (*this)[row][z] * value); }
}

template <class type> void inline mat<type>::DivideRow(const uint& row, const double& value)
{   // Divides the given row by the value provided
    for (uint z = 0; z < this->m_Columns; z++) { Edit_Member(row, z, (*this)[row][z] / value); }
}

template <class type> void inline mat<type>::AddRows(const uint& r1, const uint& r2)
{   // Adds row r2 to row r1
    for (uint z = 0; z < this->m_Columns; z++) { Edit_Member(r1, z, (*this)[r1][z]+(*this)[r2][z]); }
}

template <class type> void inline mat<type>::SubtractRows(const uint& r1, const uint& r2)
{   // Subtracts row r2 from row r1
    for (uint z = 0; z < this->m_Columns; z++) { Edit_Member(r1, z, (*this)[r1][z]-(*this)[r2][z]); }
}

template <class type> const uint inline mat<type>::FirstInRC(const double& number, const uint& startpos, const bool& R, const bool& C) const
{   /* Find the first time a number appears as the first entry in a row or column - by default, this function will look for the first time an entry appears in a row */
    assert(startpos >= 0 && startpos < this->m_Rows);
    for (uint i = startpos; i < this->m_Rows; i++)
    {
        if (R == true && (*this)[i][0] == number) { return i; }
        else if (C == true && (*this)[0][i]) { return i; }
    }
    // Pivot failed - default row to pivot is 1
    return 1;
}

#endif /* mat_inl */
