//
//  mat_base_impl.h
//  mat_template
//
//  Created by Hidde Boekema on 27/02/2018.
//  Copyright © 2018 Hidde Boekema. All rights reserved.
//

#ifndef mat_base_inl
#define mat_base_inl

#include "mat_base.h"

#include <iostream>
#include <vector>
#include "checks.h"
#include "global.h"

// Header file with (abstract) template base class definitions

template <class type> Base<type>::Base(const std::vector<type>& M, const uint& R, const uint& C):
m_Rows(R), m_Columns(C), Members(M)
{ m_Size = R * C; }

template <class type> Base<type>::Base(const Base& to_copy)
{   // Explicit copy constructor
    m_Rows = to_copy.m_Rows;
    m_Columns = to_copy.m_Columns;
    m_Size = to_copy.m_Size;
    Members = to_copy.Members;
}

template <class type> Base<type>& Base<type>::operator=(const Base<type>& matrix)
{   // Explicit copy assignment operator
    if (this != &matrix)
    {
        m_Rows = matrix.m_Rows;
        m_Columns = matrix.m_Columns;
        m_Size = matrix.m_Size;
        Members = matrix.Members;
    }
    
    return *this;
}

template <class type> Base<type>& Base<type>::operator=(const Base<type>&& matrix)
{   // Explicit move assignment operator
    m_Rows = std::move(matrix.m_Rows);
    m_Columns = std::move(matrix.m_Columns);
    m_Size = std::move(matrix.m_Size);
    Members = std::move(matrix.Members);
    
    return *this;
}

// Accessor member functions for protected class data members
template <class type> inline type Base<type>::Get_Member(const int& i, const int& j) const
{
    return (*this)[i][j];
}

template <class type> inline std::vector<type> Base<type>::Get_Members() const
{
    return Members;
}

template <class type> inline uint Base<type>::Get_Rows() const
{
    return m_Rows;
}

template <class type> inline uint Base<type>::Get_Columns() const
{
    return m_Columns;
}

// Member adjustment member functions for protected class data members
template <class type> inline void Base<type>::Set_RC(const uint& R, const uint& C)
{   /* Changes the rows and columns of the matrix or vector. Note: if a vector is changed to a matrix, it will be converted from vec<type> to mat<type> */
    m_Rows = R;
    m_Columns = C;
}

template <class type> void Base<type>::Edit_Member(const uint& i, const uint& j, const type& value)
{   // Changes the specified member to the value provided
    uint rows = m_Rows;
    uint columns = m_Columns;
    assert(i <= rows && j <= columns);
    std::vector<type> v_temp = Get_Members();
    
    v_temp[i * columns + j] = value;
    Members = v_temp;
}

template <class type> inline void Base<type>::Increment_Member(const uint& i, const uint& j, const type& value)
{   // Can be used to increment or decrement a member easily
    Edit_Member(i, j, (*this)[i][j] + value);
}

template <class type> inline void Base<type>::Multiply_Member(const uint& i, const uint& j, const type& value)
{   // Can be used to multiply or divide a member easily
    Edit_Member(i, j, (*this)[i][j] * value);
}

// Proxy class constructor
template <class type> Base<type>::Proxy::Proxy(const std::vector<type>& R):
m_Mat_Row(R)
{}

template <class type> inline type Base<type>::Proxy::operator[](const int& j) const
{   // Function that returns the member j in row i (of Members)
    assert(j < m_Mat_Row.size());
    return m_Mat_Row[j];
}

template <class type> typename Base<type>::Proxy Base<type>::operator[](const int& i) const
{   // Function that returns Proxy object containing row i (of Members)
    std::vector<type> temp;
    for (int k = 0; k < m_Columns; k++)
    {
        temp.push_back(Members[m_Columns * i + k]);
    }
    
    return Proxy(temp);
}

template <class type> inline void Base<type>::Erase(const bool& updateRC)
{   // Erases the members of the matrix and leaves it empty - option to delete row and column info as well
    Members.clear();
    UpdateSize(updateRC);
}

template <class type> inline void Base<type>::UpdateSize(const bool& updateRC)
{   /* Updates the m_Size member of the matrix - beware of the fact that m_Rows and m_Columns stay the same if updateRC is false */
    m_Size = static_cast<uint>(Members.size());
    
    if (updateRC)
    {
        m_Rows = 0;
        m_Columns = 0;
    }
}

#endif /* mat_base_inl */
