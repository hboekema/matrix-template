//
//  global.h
//  mat_template
//
//  Created by Hidde Boekema on 14/05/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#ifndef global_h
#define global_h

#include <typeinfo>
#include <iostream>

class vector;

// Global constant pointers to the type_info of standard types
extern const std::type_info* type_bool;
extern const std::type_info* type_double;
extern const std::type_info* type_int;
extern const std::type_info* type_long;
extern const std::type_info* type_short;
extern const std::type_info* type_uint;
extern const std::type_info* type_ulong;
extern const std::type_info* type_ushort;

// Cluster special/similar types
extern const std::vector<const std::type_info*> all_types;
extern const std::vector<const std::type_info*> numerical_types;

// Classless functions that may be of use
std::vector<uint> create_identity(const uint& n);

// Classless functions that may be of use
std::vector<uint> create_identity(const uint& n)
{   // Creates and returns an n * n identity matrix vector
    std::vector<uint> I_vector;
    
    for (uint i = 0; i < n; i++)
    {
        for (uint j = 0; j < n; j++)
        {
            if (i == j)
            {   // If along the leading diagonal, this entry should be 1. Otherwise, it should be 0.
                I_vector.push_back(1);
            } else {
                I_vector.push_back(0);
            }
        }
    }
    
    return I_vector;
}

#endif /* global_h */
