//
//  vec.h
//  mat_template
//
//  Created by Hidde Boekema on 16/04/2017.
//  Copyright © 2017 Hidde Boekema. All rights reserved.
//

#ifndef vec_h
#define vec_h


#include <cassert>
#include "mat_base.h"

class vector;

template <class type> class vec : public Base<type>
{
    // Derived class for mathematical vectors
public:
    vec(const std::vector<type>& V = {0, 0, 0}, const bool& T = false);
    vec Trans() const;
    
    // Overloaded operators for vector operations
    float operator*(const vec& other) const;
    vec operator^(const vec& other) const;
private:
    const vec* Transpose() const;
};

#include "vec.inl"

#endif /* vec_h */
